#include <iostream>
#include <time.h>

size_t          SumElemOfArray(int* Array, int N)
{
    size_t      Sum = 0;

    for (size_t i = 0; i < N; i++)
        Sum += Array[i];

    return Sum;
}

int             main()
{
    const int   N = 10;
    int         Array[N][N];
    time_t      Timestamp;
    struct tm   Today;
    char        Date[128];
    size_t      Sum;
    int         Index;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Array[i][j] = i + j;
            std::cout << Array[i][j];
            std::cout << ' ';
        }
        std::cout << '\n';
    }

    std::cout << '\n';

    _tzset();
    _strdate_s(Date, 128);
    std::cout << "Today: " << Date << '\n';
    std::cout << "N = " << N << '\n';

    time(&Timestamp);
    _localtime64_s(&Today, &Timestamp);
    Index = (int)Today.tm_mday % N;
    Sum = SumElemOfArray(Array[Index], N);
    std::cout << "Sum elem of line #" << Index << ": " << Sum;
    std::cout << '\n';

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
